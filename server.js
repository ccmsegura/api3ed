/*importamos express*/
var express = require('express')
var app = express()
var requestJson = require ('request-json')


/*definimos puerto de escucha*/
var port = process.env.PORT || 3000
/*importamos bibliotecas*/
var fs = require('fs')

var bodyParser = require('body-parser')
app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancocms/collections"
var apiKey = "apiKey=kJAwhCULtWcYjMPzIG1nEA00GH6wo4AA"
var clienteMlab = null

app.get('/apitechu/v5/usuarios', function(req,res){
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err,resM,body){
    if(!err)
      res.send(body)
  })
})

app.get('/apitechu/v5/usuarios/:id', function(req,res){
  var id = req.params.id
  var query = 'q={"id" : ' + id + ' }&f={"first_name":1, "last_name":1, "_id":0}'
//_id = 0 es para que no devuelva el id, por defecto lo devuelve
// f es para decir que campos, fields, quiero
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
  clienteMlab.get('', function(err,resM,body){
    if(!err){
        if (body.length > 0)
          res.send(body[0])
          // res.send({"first_name":body[0].first_name, "last_name":body[0].last_name})
        else {
          res.status(404).send('Usuario no encontrado')
        }
    }
  })
})

app.post('/apitechu/v5/usuarios/login', function(req,res){
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) //login ok
      {
        console.log(body)
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put ( '?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          console.log()
          res.send({"login":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name})})
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


app.post('/apitechu/v5/usuarios/logout', function(req,res){
  var id = req.headers.id
  console.log(id)
  var query = 'q={"id":' + id + ',"logged" : true }'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) //estaba logado
      {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put ( '?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          res.send({"logout":"ok", "id":body[0].id})})
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


app.get('/apitechu/v5/cuentas', function(req,res){
  var id = req.headers.idcliente
  var query = 'q={"idcliente": ' + id + '} &f={"iban":1, "_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
          res.send(body)
      }
      else {
        res.status(404).send('Usuario no tiene cuenta')
      }
  })
})

app.get('/apitechu/v5/movimientos', function(req,res){
  var iban = req.headers.iban
  var query = 'q={"iban": "' +  iban  + '"} &f={"movimientos":1, "_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
          res.send(body)
      }
      else {
        res.status(404).send('Usuario no tiene cuenta')
      }
  })
})

/*app.post('/apitechu/v5/cuentas', function(req,res){
  var id = req.headers.idcliente
  var query = 'q={"idcliente": ' + id + '}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length >= 1) //tiene cuenta
      {
          res.send({ "iban":body[0].iban})
      }
      else {
        res.status(404).send('Usuario no tiene cuenta')
      }
    }
  })
})*/

app.listen(port)

console.log("API escuchando en el puerto" + port)

var usuarios = require('./usuarios.json')
var usLogin = require('./usuarios3.json')
var cuentas = require('./cuentas.json')

console.log("Hola Mundo")

app.get('/apitechu/v1',function(req,res){
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req,res){
  res.send(usuarios)

})

app.post('/apitechu/v1/usuarios', function(req,res){
  var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios-2.json", datos, "utf8", function(err) {
    if (err)
      console.log(err)
    else
      console.log("Fichero guardado")
  })
  res.send("Alta OK")
})

app.post('/apitechu/v2/usuarios', function(req,res){
  /*var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
  usuarios.push(nuevo)*/
  var nuevo = req.body
  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err)
      console.log(err)
    else
      console.log("Fichero guardado")
  })
  res.send("Alta OK")
})


app.delete('/apitechu/v1/usuarios/:elegido', function(req,res){
    usuarios.splice(req.params.elegido-1, 1)
    res.send("Usuario Borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){
  console.log("Parámetros")
  console.log(req.params)
  console.log("Query Strings")
  console.log(req.query)
  console.log("Query Headers")
  console.log(req.headers)
  console.log("Query Body")
  console.log(req.body)
  res.send("Obtener datos")
})

app.post('/apitechu/v2/usuarios/login', function(req,res){
  var email = req.headers.email
  var pw = req.headers.password
  var idusuario = 0
  for (var i = 0; i < usLogin.length; i++) {
    if(usLogin[i].email == email && usLogin[i].password == pw){
      idusuario = usLogin[i].id
      usLogin[i].logged = true
      break;
    }
  }
  if (idusuario != 0){

    const datos = JSON.stringify(usLogin)
    fs.writeFile("./usuarios3.json", datos, "utf8", function(err) {
      if (err)
        console.log(err)
      else
        console.log("Fichero guardado")
    })
  }
    else
      res.send("usuario no encontrado")

    res.send("login ok, usuario id: " + idusuario)
})

app.post('/apitechu/v2/usuarios/logout', function(req,res){
  var idusuario = req.headers.id
  var usuario_logado = false
  for (var i = 0; i < usLogin.length; i++) {
    if(usLogin[i].id == idusuario && usLogin[i].logged == true){
      usuario_logado = true
      usLogin[i].logged = false
      break;
    }
  }
  if (usuario_logado)
    res.send({"logout" : "sí", "id": idusuario})
  else
    res.send({"logout":"no", "msj":"el usuario no había iniciado sesión"})
})

app.get('/apitechu/v3/cuentas', function(req,res){
  var array = []
  for (var i = 0; i < cuentas.length; i++) {
      array.push(cuentas[i].iban)
  }
  res.send(array)
})

app.get('/apitechu/v3/cuentas/movimientos', function(req,res){
  var iban = req.headers.iban
  var movimientoscliente = []
  for (var i = 0; i < cuentas.length; i++) {
    if(cuentas[i].iban == iban){
      movimientoscliente = (cuentas[i].movimientos)
      break;
    }
  }
    res.send(movimientoscliente)
})

app.get('/apitechu/v3/cuentas/usuario', function(req,res){
  var cliente = req.headers.idcliente
  var ibancliente = []
  for (var i = 0; i < cuentas.length; i++) {
    if(cuentas[i].idcliente == cliente){
      ibancliente.push(cuentas[i].iban)
    }
  }
    res.send(ibancliente)
})
